import java.util.*;
import java.util.Scanner;

public class psuedoChat {

	
	public static void main(String[] args){
		Random rand = new Random();
		Scanner s = new Scanner(System.in);
	    String computer = "computer: ";
	    String user = "me: ";
	    String[] cResponse = {"I like pie",
	    		"Hello, my name is",
	    		"Java's got nothing on me",
	    		"OOP is not always the solution",
	    		"Blastoff! (straight from the book, here)", 
	    		"Floating point errors are completely avoidable."};
	    String req = "  ";
	    while (true){
	    	System.out.print(user);
	    	req = s.nextLine();
	    	if (req.equals("")){
	    		System.out.println("computer: Goodbye!");
	    		break;
	    	}
	    	System.out.print(computer);
	    	int randomNumber = rand.nextInt(6);
	    	System.out.print(cResponse[randomNumber]);
	    	System.out.println(" ");
	    }
	    s.close();
	}

}
